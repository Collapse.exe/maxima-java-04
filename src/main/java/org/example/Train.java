package org.example;

import lombok.*;

@AllArgsConstructor()
@NoArgsConstructor
@Getter
@Setter
@ToString()
public class Train extends Transport {
    private String id;
    private int carriageCount;
    private boolean isExpress;

    public float getPrice(City city) {
        return getCostOfKm() * city.getDistanceKm();
    }
}
